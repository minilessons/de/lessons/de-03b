
<p>Booleovu algebru definirat ćemo navođenjem niza aksioma i postulata (to su tvrdnje koje ne dokazujemo, već postuliramo da one vrijede). Njihovom uporabom potom možemo dokazivati teoreme. Prilikom formalizacije, želja nam je pronaći minimalni skup postulata koji su međusobno nezavisni (niti jedan nije posljedica drugih) i koji su dovoljni da bismo mogli dokazati sve što vrijedi u promatranoj algebri. Stoga ćemo u nastavku prikazati aksiomatizaciju Booleove algebre Huntingtonovim postulatima kako je Huntington napravio 1904. godine. Danas znamo da ta aksiomatizacija nije minimalna: sam Huntington je 1933. dao <a href="https://en.wikipedia.org/wiki/Edward_Vermilye_Huntington" target="_blank">manju aksiomatizaciju</a>. Međutim, aksiomatizacija iz 1904. godine počiva na operatorima i aksiomima koje i danas izravno koristimo pa nam je stoga puno zanimljivija.</p>

<p>Aksiomatizacija koja je ovdje dana izravno je preuzeta iz Huntingtonovog rada <a href="http://www.ams.org/journals/tran/1904-005-03/S0002-9947-1904-1500675-4/S0002-9947-1904-1500675-4.pdf" target="_blank"><em>Set of Independent Postulates for the Algebra of Logic</em></a> objavljenog 1904. godine i mrvicu je različita od one dane u slideovima s predavanja.</p>

<p>Označimo s <i>K={a,b,c,...}</i> skup objekata nad kojim su definirana dva binarna operatora \(+\) i \(\cdot\) te jedan unarni operator \(\bar ~\).

<ul class="mlSpread">
<li><span class="li-inline-definition">Postulat 1.</span> Operatori \(+\) i \(\cdot\) su zatvoreni nad skupom <i>K</i>.<br>
a) \((\forall a,b \in K) \quad a+b \in K\)<br>
b) \((\forall a,b \in K) \quad a \cdot b \in K\)<br>
<em>Zatvorenost</em> operatora s obzirom na neki skup znači da će rezultat biti u tom skupu kad god su operandi iz tog skupa. U klasičnoj algebri, operator zbrajanja nad prirodnim brojevima je zatvoren nad skupom prirodnih brojeva; operator oduzimanja to nije. Primjerice, 2-1 je ponovno prirodni broj ali 1-2 nije. U postulatizaciji Booleove algebre koju ovdje dajemo zahtjeva se da oba operatora budu zatvorena s obzirom na obje binarne operacije.
</li>
<li><span class="li-inline-definition">Postulat 2.</span> Postojanje neutralnog elementa. Zahtjeva se da u skupu <i>K</i> postoje dva elementa, označit ćemo ih s 0 i 1, za koje vrijedi:<br>
a) \((\forall a \in K) \quad a + 0 = a\)<br>
b) \((\forall a \in K) \quad a \cdot 1 = a\)<br>
Element 0 se naziva <em>neutralni element s obzirom na operator \(+\)</em> a  element 1 se naziva <em>neutralni element s obzirom na operator \(\cdot\)</em>. U klasičnoj algebri, neutralni element koristi se i pri definiciji inverznog elementa: kako je 0 neutralni element za zbrajanje, inverzni element od <i>a</i> je <i>b</i> ako za njega vrijedi <i>a</i>+<i>b</i>=0; takav <i>b</i> označavamo s <i>-a</i> i nazivamo inverzni element od <i>a</i> (konkretno, aditivni inverz; slično definiramo multiplikativni inverz). U Booleovoj algebri ne postoje inverzni elementi.
</li>
<li><span class="li-inline-definition">Postulat 3.</span> Operatori \(+\) i \(\cdot\) su komutativni.<br>
a) \((\forall a,b \in K) \quad a + b = b + a\)<br>
b) \((\forall a,b \in K) \quad a \cdot b = b \cdot a\)<br>
Komutativnost traži da rezultat ostane isti ako se zamijene operandi. U klasičnoj algebri imamo primjere operatora koji jesu komutativni i koji nisu komutativni; primjerice, operatori zbrajanja i množenja jesu komutativni (3+2=2+3, 3*2=2*3) ali operatori oduzimanja i dijeljenja nisu (3-2&#x2260;2-3, 3/2&#x2260;2/3). 
</li>
<li><span class="li-inline-definition">Postulat 4.</span> Operatori \(+\) i \(\cdot\) su međusobno distributivni.<br>
a) \((\forall a,b,c \in K) \quad a + (b \cdot c) = (a + b) \cdot (a + c)\)<br>
b) \((\forall a,b,c \in K) \quad a \cdot (b + c) = (a \cdot b) + (a \cdot c)\)<br>
U klasičnoj algebri, operator \(\cdot\) je distributivan preko operatora \(+\) ali obrat ne vrijedi. U Booleovoj algebri zahtjeva se da su oba binarna operatora međusobno distributivna.
</li>
<li><span class="li-inline-definition">Postulat 5.</span> Definicija komplementa.<br>
a) \((\forall a \in K)(\exists \bar a \in K) \quad a + \bar a = 1\)<br>
b) \((\forall a \in K)(\exists \bar a \in K) \quad a \cdot \bar a = 0\)<br>
pri čemu je 1 neutralni element za operator \( \cdot \) a 0 neutralni element za operator \( + \). Uočite stoga da \(a\) i \(\bar a\) nisu međusobno inverzni element - taj pojam označava nešto drugo i u Booleovoj algebri ne postoji. Za njih umjesto toga kažemo da su međusobno komplementarni, odnosno da je element \(\bar a\) komplement od elementa \(a\).
</li>
<li><span class="li-inline-definition">Postulat 6.</span> Skup <i>K</i> ima barem dva različita elementa.<br>
\((\exists a,b \in K) \quad a \neq b\)<br>
</li>
</ul> 

<p>Uočite kako ova postulatizacija definira Booleovu algebru nad skupom <i>K</i> koji može imati mnogo elemenata. Međutim, u skladu s napomenom s prethodne stranice gdje smo govorili o Shannonu i činjenicom da trebamo formalizam za opisivanje digitalnog sklopovlja koje na elementarnoj razini može biti u jednom od samo dva moguća stanja, najčešće korištena Booleova algebra, odnosno ono što podrazumijevamo pod pojmom Booleova algebra ako drugačije nije rečeno, jest Booleova algebra koja je izgrađena nad dvočlanim skupom <i>K</i>={0,1}. Postulat 2 traži da u skupu <i>K</i> postoje neutralni element za svaki od dva binarna operatora; postulat 5 definira djelovanje komplementa oslanjajući se na neutralne elemente dok postulat 6 traži da u skupu <i>K</i> postoje barem dva različita elementa. Minimalni skup <i>K</i> koji zadovoljava sve ove postulate jest upravo skup koji sadrži samo spomenuta dva neutralna elementa.</p>

<h3>Dvovrijednosna Booleova algebra</h3>

<p>Dvovrijednosna Booleova algebra (engl. <i>two-valued Boolean Algebra</i>, <i>Switching algebra</i> ili ponekad čak i <i>binary logic</i>) je Booleova algebra definirana nad skupom <i>K</i>={0,1} koja zadovoljava svih šest Huntingtonovih postulata te ima dva binarna i unarni operator definirane tablicama u nastavku.</p>

<table class="mlTableCC">
<tr>
<td style="padding-right: 20px;">
Binarni operator \(+\)
<table class="mlTableCC mlTableVR2">
<thead><tr><th>\(a\)</th><th>\(b\)</th><th>\(a+b\)</th></tr></thead>
<tbody>
<tr><td>0</td><td>0</td><td>0</td></tr>
<tr><td>0</td><td>1</td><td>1</td></tr>
<tr><td>1</td><td>0</td><td>1</td></tr>
<tr><td>1</td><td>1</td><td>1</td></tr>
</tbody>
</table>
</td>
<td style="padding-right: 20px;">
Binarni operator \(\cdot\)
<table class="mlTableCC mlTableVR2">
<thead><tr><th>\(a\)</th><th>\(b\)</th><th>\(a \cdot b\)</th></tr></thead>
<tbody>
<tr><td>0</td><td>0</td><td>0</td></tr>
<tr><td>0</td><td>1</td><td>0</td></tr>
<tr><td>1</td><td>0</td><td>0</td></tr>
<tr><td>1</td><td>1</td><td>1</td></tr>
</tbody>
</table>
</td>
<td style="vertical-align: top;">
Binarni operator \(\bar ~\)
<table class="mlTableCC mlTableVR2">
<thead><tr><th>\(a\)</th><th>\(\bar a\)</th></tr></thead>
<tbody>
<tr><td>0</td><td>1</td></tr>
<tr><td>1</td><td>0</td></tr>
</tbody>
</table>
</td>
</tr>
</table>

<p>Ovo je algebra s kojom ćemo se družiti ostatak semestra.</p>

<h3>Metateorem o dualnosti</h3>

<p>Jedan iznimno važan pojam uz definiciju Booleove algebre jest i metateorem o dualnosti. Prije no što ga iskažemo, pojasnimo najprije kako se definira dualni izraz.</p>

<div class="mlImportant">Označimo s <i>B</i> bilo kakav izraz izgrađen od elemenata skupa {0,1} te definiranih binarnih i unarnog operatora; primjerice, \((a \cdot 1) + (\bar b \cdot \bar 0)\). Oznakom <i>B</i><sub>D</sub> označavat ćemo njegov dualni izraz. <em>Dualni</em> izraz dobiva se tako da se:
<ul>
<li>međusobno zamijene neutralni elementi 0 i 1 te</li>
<li>međusobno zamijene binarni operatori \(+\) i \(\cdot\).</li>
</ul>
<p>Prema toj definiciji, dualni izraz izraza \((a \cdot 1) + (\bar b \cdot \bar 0)\) je izraz \((a + 0) \cdot (\bar b + \bar 1)\).
</p>
</div>

<p>Sada smo spremni za izricanje metateorema o dualnosti.</p>

<div class="mlImportant"><b>Metateorem o dualnosti</b><br>
Ako je neki izraz aksiom, postulat ili teorem Booleove algebre, onda je to i njegov dualni izraz.
</div>

<p>I doista, pogledajte već i same postulate: postulati 1, 2, 3, 4 i 5 svaki daje po dva zahtjeva koja moraju vrijediti. Uočite da su ti zahtjevi međusobno dualni.</p>

<h3>Važniji teoremi Booleove algebre</h3>

<p>U nastavku ćemo navesti i nekoliko važnijih teorema Booleove algebre. Sjetite se: teoremi u ovom kontekstu su izrazi koje je moguće dokazati pozivajući se na dane postulate.</p>

<ol class="mlSpread">
<li><div class="li-definition">U Booleovoj algebri svaki element ima jedinstven komplement.</div>
Evo dokaza tvrdnje. Pretpostavimo da \(a\) ima dva komplementa: \(\bar a_1 \) i \(\bar a_2 \). Ako su oni doista komplementi, onda moraju zadovoljavati postulat 5, pa tada sigurno vrijedi \(a + \bar a_1 = 1\), \(a \cdot \bar a_1 = 0\), \(a + \bar a_2 = 1\) i konačno  \(a \cdot \bar a_2 = 0\). Obratite pažnju na ove izraze - i njih ćemo koristiti u dokazu tvrdnje koji slijedi. Možemo pisati:
$$
\begin{align}
 \bar a_1 &amp;= \bar a_1 \cdot 1 &amp;&amp; \text{1 je neutralni element, postulat 2b} \\
          &amp;= \bar a_1 \cdot (a + \bar a_2) &amp;&amp; \text{zamjenom } a + \bar a_2 = 1 \\
          &amp;= (\bar a_1 \cdot a) + (\bar a_1 \cdot \bar a_2) &amp;&amp; \text{distributivnost, postulat 4b} \\
          &amp;= (a \cdot \bar a_1) + (\bar a_1 \cdot \bar a_2) &amp;&amp; \text{komutativnost, postulat 3b} \\
          &amp;= 0 + (\bar a_1 \cdot \bar a_2) &amp;&amp; \text{zamjenom } a \cdot \bar a_1 = 0\\
          &amp;= (a \cdot \bar a_2) + (\bar a_1 \cdot \bar a_2) &amp;&amp; \text{zamjenom } a \cdot \bar a_2 = 0\\
          &amp;= (\bar a_2 \cdot a) + (\bar a_1 \cdot \bar a_2) &amp;&amp; \text{komutativnost, postulat 3b} \\
          &amp;= (\bar a_2 \cdot a) + (\bar a_2 \cdot \bar a_1) &amp;&amp; \text{komutativnost, postulat 3b} \\
          &amp;= \bar a_2 \cdot (a + \bar a_1) &amp;&amp; \text{distributivnost, postulat 4b} \\
          &amp;= \bar a_2 \cdot 1 &amp;&amp; \text{zamjenom } a + \bar a_1 = 1\\
          &amp;= \bar a_2 &amp;&amp; \text{1 je neutralni element, postulat 2b}\\
\end{align}
$$
<p>Ovime smo dokazali da ako dva elementa zadovoljavaju svojstvo komplementarnosti prema trećem elementu, onda su ta dva elementa jednaka (pa zapravo nisu dva već jedan te isti element). Time smo pokazali da svaki element ima svoj jedinstveni komplement.</p>
</li>
<li><div class="li-definition">Dominacija.</div>
$$
\begin{align}
a + 1 &amp;= 1 \\
a \cdot 0 &amp;= 0\\
\end{align}
$$
<p>Za vježbu ćemo još pokazati kako dokazati prvi dio teorema. Ostale teoreme nećemo dokazivati.</p>
$$
\begin{align}
      a+1 &amp;= (a+1) \cdot 1 &amp;&amp; \text{1 je neutralni element, postulat 2b} \\
          &amp;= (a+1) \cdot (a + \bar a) &amp;&amp; \text{svojstvo komplementa, postulat 5a} \\
          &amp;= a + (1 \cdot \bar a) &amp;&amp; \text{distributivnost, postulat 4a} \\
          &amp;= a + (\bar a \cdot 1) &amp;&amp; \text{komutativnost, postulat 3b} \\
          &amp;= a + \bar a &amp;&amp; \text{1 je neutralni element, postulat 2b}\\
          &amp;= 1 &amp;&amp; \text{svojstvo komplementa, postulat 5a}\\
\end{align}
$$
</li>
<li><div class="li-definition">Idempotencija.</div>
$$
\begin{align}
a + a &amp;= a \\
a \cdot a &amp;= a\\
\end{align}
$$
</li>
<li><div class="li-definition">Involutivnost komplementa.</div>
$$
\bar {\bar a} = a
$$
</li>
<li><div class="li-definition">Apsorpcija.</div>
$$
\begin{align}
a + a \cdot b &amp;= a \\
a \cdot (a + b) &amp;= a \\
\end{align}
$$
</li>
<li><div class="li-definition">Asocijativnost.</div>
$$
\begin{align}
(a + b) + c &amp;= a + (b + c) \\
(a \cdot b) \cdot c &amp;= a \cdot (b \cdot c) \\
\end{align}
$$
</li>
<li><div class="li-definition">de Morganovi zakoni.</div>
$$
\begin{align}
\overline {a + b} &amp;= \bar a \cdot \bar b \\
\overline {a \cdot b} &amp;= \bar a + \bar b \\
\end{align}
$$
Napomena: vrijedi i za više varijabli.
$$
\begin{align}
\overline {a + b + \cdots} &amp;= \bar a \cdot \bar b \cdot \cdots \\
\overline {a \cdot b \cdot \cdots} &amp;= \bar a + \bar b + \cdots\\
\end{align}
$$
</li>
<li><div class="li-definition">Simplifikacija.</div>
$$
\begin{align}
a \cdot b + a \cdot \bar b = a \\
(a + b) \cdot (a + \bar b) = a \\
\end{align}
$$
Ovaj teorem od posebnog je značaja jer nam izravno govori na koji se način složeniji izraz za čiju realizaciju trebamo više logičkih sklopova može ostvariti jednostavnije. Teorem je temelj za postupke minimizacije s kojima ćemo se upoznati relativno brzo.
</li>
<li><div class="li-definition">Neutralni element jednog binarnog operatora je komplement neutralnog elementa onog drugog binarnog operatora.</div>
$$
\begin{align}
\bar 0 &amp;= 1 \\
\bar 1 &amp;= 0 \\
\end{align}
$$
Dokaz ovog teorema je trivijalan:
$$
\begin{align}
\bar 0 &amp;= \bar 0 + 0 &amp;&amp; \text{0 je neutralni element za zbrajanje, postulat 2a} \\
       &amp;= 0 + \bar 0 &amp;&amp; \text{komutativnost, postulat 3a}\\
       &amp;= 1 &amp;&amp; \text{svojstvo komplementa, postulat 5a}\\
\end{align}
$$
<p>Na analogni način se može dokazati i drugi dio. Alternativa: primijenite metateorem o dualnosti.</p>
</li>
</ol>






